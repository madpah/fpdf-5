<?php
/**
 * Contians the FPDF5_Image_PNG class.
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5
 * @see http://www.fpdf.org
 * @version $Id: $
 */
namespace com\phpsystems\fpdf5\image;

/**
 * FPDF-5 Image class for images of type PNG.
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5.Image
 */
class FPDF5_Image_PNG extends FPDF5_Image {

   /**
    * Parse image data.
    *
    * @throws FPDF5_Exception
    */
   protected function parse_image_data() {
      $this->str_image_data = '';
      $res_image = @fopen($this->get_image_filename(), 'rb');
      if ($res_image !== false) {
         // Check PNG signature
         if ($this->read_stream($res_image, 8) != chr(137) . 'PNG' . chr(13) . chr(10) . chr(26) . chr(10)) {
            throw new FPDF5_Exception(__METHOD__ . ': Image file does not contain a PNG: ' . $this->get_image_filename());
         }

         $this->read_stream($res_image, 4);
         if ($this->read_stream($res_image, 4) != 'IHDR') {
            throw new FPDF5_Exception(__METHOD__ . ': Incorrect PNG file: ' . $this->get_image_filename());
         }

//         while (!feof($res_image)) {
//            $this->str_image_data .= fread($res_image, 8192);
//         }
//         fclose($res_image);
      } else {
         throw new FPDF5_Exception(__METHOD__ . ': Unable to open image file: ' . $this->get_image_filename());
      }
   }
}