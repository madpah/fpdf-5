<?php
/**
 * Sample 1 - FPDF-5
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-10
 * @package FPDF5
 * @see http://fpdf5.phpsystems.co.uk
 * @version $Id: $
 */

require_once('../FPDF5.class.php');
require_once('../fonts/Helvetica.font.php');

$obj_pdf = new FPDF5();
$obj_pdf->add_page();
$obj_font = new FPDF5_Font_Helvetica();
$obj_font->set_size(16);
$obj_font->set_underlined(true);
$obj_pdf->set_font($obj_font);
$obj_pdf->set_text_colour(new FPDF5_Colour(255, 10, 0));
$obj_pdf->draw_cell(
   FPDF5_Unit::create(40, FPDF5_Unit::UNIT_MM),
   FPDF5_Unit::create(10, FPDF5_Unit::UNIT_MM),
   'Hello World!',
   0,
   '',
   'LRB',
   false,
   null
);
$obj_pdf->draw_cell(
   FPDF5_Unit::create(40, FPDF5_Unit::UNIT_MM),
   FPDF5_Unit::create(10, FPDF5_Unit::UNIT_MM),
   'Hello World!',
   1,
   '',
   '',
   false,
   null
);
$obj_pdf->draw_cell(
   FPDF5_Unit::create(40, FPDF5_Unit::UNIT_MM),
   FPDF5_Unit::create(10, FPDF5_Unit::UNIT_MM),
   'Hello World!',
   1,
   '',
   '',
   false,
   null
);
$obj_font = new FPDF5_Font_Helvetica();
$obj_font->set_size(24);
$obj_font->set_underlined(false);
$obj_pdf->set_font($obj_font);
$obj_pdf->draw_cell(
   FPDF5_Unit::create(40, FPDF5_Unit::UNIT_MM),
   FPDF5_Unit::create(10, FPDF5_Unit::UNIT_MM),
   'Hello World!',
   1,
   '',
   'TRL',
   false,
   null
);
$obj_pdf->output();