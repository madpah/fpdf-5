<?php
/**
 * Sample 2 - FPDF-5
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-10
 * @package FPDF5
 * @see http://fpdf5.phpsystems.co.uk
 * @version $Id: $
 */

require_once('../FPDF5.class.php');
require_once('../fonts/Helvetica.font.php');

/**
 * Concretete PDF class.
 *
 */
class MyPDF extends FPDF5 {

   /**
    * Header Content.
    *
    */
   protected function draw_header() {
      parent::draw_header();

      $obj_image = FPDF5_Image::create('images/WATERLILY.jpg');
      $obj_image->set_render_width(FPDF5_Unit::create(33, FPDF5_Unit::UNIT_MM));
      $this->draw_image($obj_image, 10, 8);
      // $this->Image('logo_pb.png',10,8,33);

      $obj_font = new FPDF5_Font_Helvetica();
      $obj_font->set_size(15);
      $this->set_font($obj_font);

      $this->draw_cell(FPDF5_Unit::create(80));

      $this->draw_cell(FPDF5_Unit::create(30), FPDF5_Unit::create(10), 'Title', 1, FPDF5::ALIGNMENT_CENTRE, '1');

//         Arial bold 15
//   	$this->SetFont('Arial','B',15);
//   	Move to the right
//   	$this->Cell(80);
//   	Title
//   	$this->Cell(30,10,'Title',1,0,'C');
   	//Line break
   	//$this->Ln(20);
   }
}

$obj_pdf = new MyPDF();
$obj_pdf->add_page();
$obj_pdf->output();