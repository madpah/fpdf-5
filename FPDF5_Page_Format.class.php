<?php
/**
 * Contians the FPDF5_Page_Format class.
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5
 * @see http://www.fpdf.org
 * @version $Id: $
 */
namespace com\phpsystems\fpdf5;

/**
 * FPDF-5 Page class.
 *
 * For handling differnet page types and sizes.
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5
 */
class FPDF5_Page_Format {

   // Default Margin is 1cm or 10mm.
   const DEFAULT_MARGIN = 10.00;

   const MARGIN_LEFT = 1;
   const MARGIN_TOP = 2;
   const MARGIN_RIGHT = 3;
   const MARGIN_BOTTOM = 4;

   const ORIENTATION_PORTRAIT = 1;
   const ORIENTATION_LANDSCAPE = 2;

   const PAGE_FORMAT_A5 = 1;
   const PAGE_FORMAT_A4 = 2;
   const PAGE_FORMAT_A3 = 3;

   /**
    * Page Definitions.
    *
    * @var Array
    */
   protected static $arr_page_definitions = array(
      self::PAGE_FORMAT_A5 => array(FPDF5_Unit::UNIT_PT, 420.94, 595.28),
      self::PAGE_FORMAT_A4 => array(FPDF5_Unit::UNIT_PT, 595.28, 841.89),
      self::PAGE_FORMAT_A3 => array(FPDF5_Unit::UNIT_PT, 841.89, 1190.55)
   );

   /**
    * Page Format.
    *
    * @var Integer
    */
   protected $int_page_format = null;

   /**
    * Page Orientation.
    *
    * @var Integer
    */
   protected $int_page_orientation = null;

   /**
    * Page Margins.
    *
    * @var Array
    */
   protected $arr_page_margins = array();

   /**
    * Get's a default FPDF5_Page_Format.
    *
    * @return FPDF5_Page_Format
    */
   public static function get_default() {
   	return new FPDF5_Page_Format(FPDF5_Page_Format::PAGE_FORMAT_A4);
   }

   /**
    * Constructor
    *
    * @param Integer $int_page_format
    */
   public function __construct($int_page_format, $int_page_orientation = FPDF5_Page_Format::ORIENTATION_PORTRAIT) {
      if (!array_key_exists($int_page_format, self::$arr_page_definitions)) {
         throw new FPDF5_Exception(__METHOD__ . ' Undefined page format specified.');
      }

      if (!in_array($int_page_orientation, array(self::ORIENTATION_PORTRAIT, self::ORIENTATION_LANDSCAPE))) {
         throw new FPDF5_Exception(__METHOD__ . ' Undefined page orientation specified.');
      }

      $this->int_page_format = $int_page_format;
      $this->int_page_orientation = $int_page_orientation;

      $this->arr_page_margins[self::MARGIN_LEFT] = FPDF5_Unit::create(self::DEFAULT_MARGIN);
      $this->arr_page_margins[self::MARGIN_TOP] = FPDF5_Unit::create(self::DEFAULT_MARGIN);
      $this->arr_page_margins[self::MARGIN_RIGHT] = FPDF5_Unit::create(self::DEFAULT_MARGIN);
      $this->arr_page_margins[self::MARGIN_BOTTOM] = FPDF5_Unit::create(self::DEFAULT_MARGIN);
   }

   /**
    * Get Height of Page.
    *
    * @return FPDF5_Unit
    */
   public function get_height() {
      if ($this->is_landscape()) {
   	   return FPDF5_Unit::create(self::$arr_page_definitions[$this->int_page_format][1], self::$arr_page_definitions[$this->int_page_format][0]);
      } else {
         return FPDF5_Unit::create(self::$arr_page_definitions[$this->int_page_format][2], self::$arr_page_definitions[$this->int_page_format][0]);
      }
   }

   /**
    * Get Page Margin at a specific position.
    *
    * @param Integer $int_margin_position
    * @return FPDF5_Unit
    */
   public function get_margin($int_margin_position = self::MARGIN_LEFT) {
   	return $this->arr_page_margins[$int_margin_position];
   }

   /**
    * Array of Page Margins.
    *
    * @return Array
    */
   public function get_margins() {
   	return $this->arr_page_margins;
   }

   /**
    * Get Page Orientation.
    *
    * @return Integer
    */
   public function get_orientation() {
   	return $this->int_page_orientation;
   }

   /**
    * Get page break trigger Y position for page format in pt.
    *
    * @return Float
    */
   public function get_page_break_trigger() {
   	return $this->get_height()->get_unit_pt_value() - $this->get_margin(self::MARGIN_BOTTOM)->get_unit_pt_value();
   }

   /**
    * Get Width of Page.
    *
    * @return FPDF5_Unit
    */
   public function get_width() {
      if ($this->is_portrait()) {
   	   return FPDF5_Unit::create(self::$arr_page_definitions[$this->int_page_format][1], self::$arr_page_definitions[$this->int_page_format][0]);
      } else {
         return FPDF5_Unit::create(self::$arr_page_definitions[$this->int_page_format][2], self::$arr_page_definitions[$this->int_page_format][0]);
      }
   }

   /**
    * Is this page orientation landscape?
    *
    * @return Boolean
    */
   public function is_landscape() {
   	return ($this->get_orientation() === self::ORIENTATION_LANDSCAPE);
   }

   /**
    * Is this page orientation portrait?
    *
    * @return Boolean
    */
   public function is_portrait() {
   	return ($this->get_orientation() === self::ORIENTATION_PORTRAIT);
   }
}