<?php
/**
 * Contians the main FPDF5 Exception class.
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5
 * @see http://www.fpdf.org
 * @version $Id: $
 */
namespace com\phpsystems\fpdf5;

/**
 * FPDF-5 Exception class.
 *
 * Generic exception thrown by FPDF5.
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5
 */
class FPDF5_Exception extends Exception {}