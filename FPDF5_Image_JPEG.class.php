<?php
/**
 * Contians the FPDF5_Image_JPEG class.
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5
 * @see http://www.fpdf.org
 * @version $Id: $
 */
namespace com\phpsystems\fpdf5\image;

/**
 * FPDF-5 Image class for images of type JPEG.
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5.Image
 */
class FPDF5_Image_JPEG extends FPDF5_Image {

   /**
    * Parse image data.
    *
    */
   protected function parse_image_data() {
      $this->str_filter = 'DCTDecode';
      $this->str_image_data = '';
      $res_image = @fopen($this->get_image_filename(), 'rb');
      if ($res_image !== false) {
         while (!feof($res_image)) {
            $this->str_image_data .= fread($res_image, 8192);
         }
         fclose($res_image);
      } else {
         throw new FPDF5_Exception('Could not read image file: ' . $this->get_image_filename());
      }
   }
}