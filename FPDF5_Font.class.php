<?php
/**
 * Contians the main FPDF5 Font class.
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5
 * @see http://www.fpdf.org
 * @version $Id: $
 */
namespace com\phpsystems\fpdf5;

/**
 * FPDF-5 Font class.
 *
 * For handling differnet font types and sizes.
 *
 * @abstract
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5
 */
abstract class FPDF5_Font {

   const TYPE_CORE = 1;
   const TYPE_TYPE_1 = 2 ;
   const TYPE_TRUE_TYPE = 3;

   const STYLE_UNDERLINE = 1;
   const STYLE_BOLD = 2;
   const STYLE_ITALIC = 4;

   /**
    * Font Type.
    *
    * @var Integer
    */
   protected $int_type = -1;

   /**
    * Font family name.
    *
    * @var String
    */
   protected $str_family = '';

   /**
    * Font Name (in PDF terms).
    *
    * @var String
    */
   protected $str_name = '';

   /**
    * Font attributes.
    *
    * @var Integer
    */
   protected $int_attributes = 0;

   /**
    * Font size.
    *
    * @var Integer
    */
   protected $int_size = 0;

   /**
    * ?
    *
    * @var Integer
    */
   protected $int_up = 0;

   /**
    * ?
    *
    * @var Integer
    */
   protected $int_ut = 0;

   /**
    * Array of char widths.
    *
    * @var Array
    */
   protected $arr_char_widths = array();

   /**
    * Constructor
    *
    */
   public function __construct() {
      $this->init();
   }

   /**
    * Font key name.
    *
    * @return String
    */
   public final function get_font_key() {
      return strtolower($this->str_family . $this->int_attributes);
   }

   /**
    * Gets width of a string in this Font.
    *
    * @param String $str_string
    * @return Integer
    */
   public final function get_string_width($str_string) {
      $str_string = (string)$str_string;
      $int_width = 0;
      for ($int_i = 0, $int_n = strlen($str_string); $int_i < $int_n; $int_i++) {
         $int_width += $this->arr_char_widths[$str_string[$int_i]];
      }
      return $int_width * $this->get_size() / 1000;
   }

   /**
    * Get Char Widths.
    *
    * @return Array
    */
   public final function get_char_widths() {
        return $this->arr_char_widths;
   }

   /**
    * Get Font Name.
    *
    * @return String
    */
   public final function get_name() {
      return $this->str_name;
   }

   /**
    * Get Font Size (pt).
    *
    * @return Integer
    */
   public function get_size() {
        return $this->int_size;
   }

   /**
    * Get Font Type.
    *
    * @return Integer
    */
   public final function get_type() {
      return $this->int_type;
   }

   /**
    * Get PDF data string to underline the given
    *
    * @param FPDF5_Page_Format $obj_page_format
    * @param Integer $int_pos_x
    * @param Integer $int_pos_y
    * @param String $str_text
    */
   public final function get_underline_pdf_data(FPDF5_Page_Format $obj_page_format, $int_pos_x, $int_pos_y, $str_text) {
      error_log("UP: {$this->get_up()}, UT: {$this->get_ut()}, W: {$this->get_string_width($str_text)}");
      error_log("({$obj_page_format->get_height()->get_unit_pt_value()} - ({$int_pos_y} - {$this->get_up()}/1000 * {$this->get_size()}))");
      return sprintf(
         '%.2F %.2F %.2F %.2F re f',
         $int_pos_x,
         ($obj_page_format->get_height()->get_unit_pt_value() - ($int_pos_y - $this->get_up()/1000 * $this->get_size())),
         $this->get_string_width($str_text),
         -$this->get_ut()/1000 * $this->get_size()
      );
   }

   /**
    * Get UP.
    *
    * @return Integer
    */
   public final function get_up() {
        return $this->int_up;
   }

   /**
    * Get UT.
    *
    * @return Integer
    */
   public final function get_ut() {
        return $this->int_ut;
   }

   /**
    * Is this font bold?
    *
    * @return Boolean
    */
   public function is_bold() {
        return ($this->int_attributes & self::STYLE_BOLD);
   }

   /**
    * Is this font italic?
    *
    * @return Boolean
    */
   public function is_italic() {
        return ($this->int_attributes & self::STYLE_ITALIC);
   }

   /**
    * Is this font underlined?
    *
    * @return Boolean
    */
   public function is_underlined() {
        return ($this->int_attributes & self::STYLE_UNDERLINE);
   }


   /**
    * Set font size (pt).
    *
    * @param Integer $int_size
    */
   public function set_size($int_size = 0) {
        $this->int_size = $int_size;
   }

   /**
    * Set or unset whether this Font is underlined.
    *
    * @param Boolean $bol_underlined
    */
   public function set_underlined($bol_underlined = true) {
        if ($bol_underlined === true && !($this->int_attributes & self::STYLE_UNDERLINE)) {
                $this->int_attributes += self::STYLE_UNDERLINE;
        } else if ($bol_underlined === false && ($this->int_attributes & self::STYLE_UNDERLINE)) {
           $this->int_attributes -= self::STYLE_UNDERLINE;
        }
   }

   /**
    * Init this font.
    *
    */
   protected abstract function init();

   /**
    * Check if the given font family is installed and available
    * for use and include if possible.
    *
    * @param String $str_font_family
    * @throws FPDF5_Exception
    * @return FPDF5_Font
    */
   public static function load_font_family($str_font_family, $int_attributes = 0) {
      $str_font_class_name = ucfirst($str_font_family);
      if ($int_attributes & self::STYLE_BOLD) {
         $str_font_class_name .= '_B';
      }
      if ($int_attributes & self::STYLE_ITALIC) {
         $str_font_class_name .= '_I';
      }

      //$str_font_include_dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'fonts' . DIRECTORY_SEPARATOR;
      //$str_font_family_include_file = $str_font_include_dir . $str_font_class_name . '.font.php';
      //if (!file_exists($str_font_family_include_file)) {
//         throw new FPDF5_Exception(__METHOD__ . ' Font Family [' . $str_font_family . '] is not installed.');
//      } else if (!is_readable($str_font_family_include_file)) {
//         throw new FPDF5_Exception(__METHOD__ . 'Font Family [' . $str_font_family . '] is installed, but can\'t be read. Check file permissions.');
//      } else {
         //require_once($str_font_family_include_file);
         // FPDF5_Font_Helvetica
         $str_font_class_name = "\com\phpsystems\fpdf5\font\{$str_font_class_name}";
         return new $str_font_class_name();
      //}
   }
}