<?php
/**
 * Contians the main FPDF5_Unit class.
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5
 * @see http://www.fpdf.org
 * @version $Id: $
 */
namespace com\phpsystems\fpdf5;

/**
 * FPDF-5 Unit class.
 *
 * For interoperating with different measurement units.
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5
 */
class FPDF5_Unit {

   const UNIT_MM = 1;
   const UNIT_CM = 2;
   const UNIT_IN = 3;

   const UNIT_PT = 10;

   /**
    * Unit conversion definitions.
    *
    * @var Array
    */
   protected $arr_unit_definitions = array(
      self::UNIT_MM => 2.8346,   // (72/25.4) = 2.8346456692913385826771653543307
      self::UNIT_CM => 28.346,   // (72/2.54) = 28.346456692913385826771653543307
      self::UNIT_IN => 72.00,
      self::UNIT_PT => 1.00
   );

   /**
    * Unit Type.
    *
    * @var Integer
    */
   protected $int_unit_type = null;

   /**
    * Unit Value.
    *
    * @var Float
    */
   protected $flt_unit_value = null;

   /**
    * Static helper method to allow for quicker creation of instances.
    *
    * @param Integer $int_unit_type
    * @param Float $flt_unit_value
    * @return FPDF5_Unit
    */
   public static function create($flt_unit_value, $int_unit_type = self::UNIT_MM) {
      return new FPDF5_Unit($flt_unit_value, $int_unit_type);
   }

   /**
    * Constructor.
    *
    * @param Integer $int_unit_type
    * @param Float $flt_unit_value
    */
   public function __construct($flt_unit_value, $int_unit_type = self::UNIT_MM) {
      if (!array_key_exists($int_unit_type, $this->arr_unit_definitions)) {
         throw new FPDF5_Exception(__METHOD__ . ' Unknown unit type specified.');
      }
      $this->int_unit_type = $int_unit_type;
      $this->flt_unit_value = $flt_unit_value;
   }

   /**
    * Get Unit type.
    *
    * @return Integer
    */
   public function get_unit_type() {
   	return $this->int_unit_type;
   }

   /**
    * Get unit value in unit type.
    *
    * @return Float
    */
   public function get_unit_value() {
   	return $this->flt_unit_value;
   }

   /**
    * Get unit value in pt.
    *
    * @return Float
    */
   public function get_unit_pt_value() {
   	return round($this->flt_unit_value * $this->arr_unit_definitions[$this->int_unit_type], 4);
   }
}