<?php

$arr_test_strings = array(
   'abc123',
   "don\'t",
   'How (about) it!',
   'A carriage ' . "\r" . ' return'
);

foreach ($arr_test_strings as $int_test => $str_test_string) {
   $str_out_a = _escape($str_test_string);
   $str_out_b = escape($str_test_string);

   echo "\r\n{$int_test}. ";
   if ($str_out_a != $str_out_b) {
      echo "FAIL: {$str_test_string}\r\n";
      echo "   FPDF 1.6 gives:   {$str_out_a}\r\n";
      echo "   FPDF-5 gives:     {$str_out_b}\r\n";
   } else {
      echo "Pass\r\n";
      echo "   : {$str_out_a} == {$str_out_b}\r\n";
   }
}

// FPDF 1.6 Version
function _escape($s)
{
	//Escape special characters in strings
	$s=str_replace('\\','\\\\',$s);
	$s=str_replace('(','\\(',$s);
	$s=str_replace(')','\\)',$s);
	$s=str_replace("\r",'\\r',$s);
	return $s;
}

// FPDF-5 Version
function escape($str_string) {
	$str_string = preg_replace(
	   array('|\\\\|', '|\(|', '|\)|', "|\\r|"),
	   array('\\\\\\\\', '\\\\(', '\\\\)', '\\\\r'),
	   $str_string
	);
	return $str_string;
}