<?php
/**
 * Contians the main FPDF-5 Colour class.
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5
 * @see http://www.fpdf.org
 * @version $Id: $
 */
namespace com\phpsystems\fpdf5;

/**
 * FPDF-5 Colour class.
 *
 * For handling and modelling colours.
 *
 * @author Paul Horton <software AT phpsystems.co.uk>
 * @copyright Paul Horton (c) 2008-9
 * @package FPDF5
 */
class FPDF5_Colour {

   /**
    * Red/Grayscale portion.
    *
    * @var Integer
    */
   protected $int_r = 0;

   /**
    * Green portion.
    *
    * @var Integer
    */
   protected $int_g = null;

   /**
    * Blue portion.
    *
    * @var Integer
    */
   protected $int_b = null;

   /**
    * Constructor.
    *
    * @param Integer $int_r
    * @param Integer $int_g
    * @param Integer $int_b
    */
   public function __construct($int_r = 0, $int_g = null, $int_b = null) {
        $this->int_r = $int_r;
        $this->int_g = $int_g;
        $this->int_b = $int_b;
   }

   /**
    * Get colour in PDF data representation.
    *
    * @return String
    */
   public function get_pdf_data() {
        if (is_null($this->int_g) && is_null($this->int_b)) {
           return sprintf('%.3F g', $this->int_r/255);
        } else {
           return sprintf('%.3F %.3F %.3F rg', $this->int_r/255, $this->int_g/255, $this->int_b/255);
        }
   }
}